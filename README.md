# po-translator
Using multiple translators' lib to translate .po files.
# Reference
* [po-autotranslate](https://github.com/NilssonOpel/po-autotranslate)
* [translate-po](https://github.com/zcribe/translate-po)
# Related lib
* [translatepy](https://github.com/Animenosekai/translate)
* [deep-translator](https://github.com/nidhaloff/deep-translator)
* [SimplyTranslate-Engines](https://codeberg.org/SimpleWeb/SimplyTranslate-Engines)
* ...
